package com.example.mylocation.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.example.mylocation.R;

public class CommonUtils {

    static ProgressDialog processing;
    static AlertDialog alertdialog;

    public static void showLoading(Context mContext, String message, boolean cancelable) {
        processing = new ProgressDialog(mContext);
        processing.setMessage(message);
        processing.setCancelable(cancelable);
        processing.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        processing.setIndeterminate(true);

        Log.d("PROGRESS DIALOG ", "******************************** PROGRESS SHOWN *************************************");
        processing.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
        processing.setCanceledOnTouchOutside(cancelable);
        if (!processing.isShowing() && !((Activity) mContext).isFinishing()) {
            processing.show();
        }
    }

    public static void hideLoading(Context context) {

        if (processing != null  && processing.isShowing() && !((Activity) context).isFinishing())
            processing.dismiss();
        Log.d("PROGRESS DIALOG ", "******************************** DISMISSED *************************************");
    }

    public static void showdefault_alertdialog(final Context context, String title, String message) {
        alertdialog = new AlertDialog.Builder(context).create();
        alertdialog.setTitle(title);
        alertdialog.setMessage(message);
        alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.btn_ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        if (alertdialog != null && !((Activity) context).isFinishing())
            alertdialog.show();
    }
}
