package com.example.mylocation.Utils;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.example.mylocation.R;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class CostService {
    private Context mContext;
    public static final String PRIMARY_CHANNEL = "default";
    private int amount = 3;
    public CostService(Context mContext) {
        this.mContext = mContext;
    }
    public void startService() {

        final Handler handler = new Handler(Looper.getMainLooper());
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> {
                    try {
                        NotificationManager notificationManager = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        String NOTIFICATION_CHANNEL_ID = "mapsNotification";
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
                            // Configure the notification channel.
                            notificationChannel.setDescription("Maps Calculate");
                            notificationChannel.enableLights(true);
                            notificationChannel.setLightColor(Color.RED);
                            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                            notificationChannel.enableVibration(true);
                            notificationManager.createNotificationChannel(notificationChannel);
                        }
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID);
                        notificationBuilder.setAutoCancel(true)
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setWhen(System.currentTimeMillis())
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setTicker("mapsNotification")
                                //.setPriority(Notification.PRIORITY_MAX)
                                .setContentTitle("Cost Calculate")
                                .setContentText("Cost is : "+calculateAmount())
                                .setContentInfo("Information");
                        notificationManager.notify(1, notificationBuilder.build());

                    } catch (Exception e) {
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 300000);
    }

    private long calculateAmount()
    {
        return System.currentTimeMillis();
    }
}
