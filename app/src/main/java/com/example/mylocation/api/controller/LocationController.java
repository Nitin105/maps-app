package com.example.mylocation.api.controller;

import android.content.Context;
import android.widget.Toast;

import com.example.mylocation.api.ApiExtensions;
import com.example.mylocation.api.ApiHandler;
import com.example.mylocation.api.LoginInterface;
import com.example.mylocation.data.RequestModel;
import com.example.mylocation.data.ResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationController extends BaseRetrofitController implements Callback<ResponseModel> {


    private final Context mContext;

    public LocationController(ApiHandler handler, int apiId, Context context) {
        super(handler, apiId);
        this.mContext = context;
    }


    public void hitNow(Object requestModel, int type) {
        apiId = type;
        switch (ApiExtensions.login.values()[apiId]) {

            case SEND_LOCATION:
                retrofit.create(LoginInterface.class)
                        .sendDetails((RequestModel) requestModel)
                        .enqueue(this);

                break;

        }

    }

    @Override
    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
        if (response.code() == 200)
            mHandler.onApiSuccess(response.body(), apiId);
        else mHandler.onApiError("Response Code---" + response.code());
    }

    @Override
    public void onFailure(Call<ResponseModel> call, Throwable t) {
        if(t.getLocalizedMessage()!=null) {

            mHandler.onApiError(t.getLocalizedMessage());
        }
        else {
            mHandler.onApiError("Can not establish connection.\nCheck your Internet Connectivity.");
        }
        Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
    }
}