package com.example.mylocation.api;

import com.example.mylocation.data.RequestModel;
import com.example.mylocation.data.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginInterface {

    String BASE_URL="https://run.mocky.io/v3/";

    @POST("1c356a09-9e1f-4b7c-b88e-1a519335a458")
    Call<ResponseModel> sendDetails(@Body RequestModel param);

}