package com.example.mylocation.api.controller;

import com.example.mylocation.api.ApiHandler;
import com.example.mylocation.api.LoginInterface;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class BaseRetrofitController {

    Retrofit retrofit;
    final ApiHandler mHandler;
    int apiId;

    BaseRetrofitController(ApiHandler handler, int apiId) {
        super();
        this.mHandler = handler;
        this.apiId = apiId;

        OkHttpClient mOkHttpClient;
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);
            mOkHttpClient = new OkHttpClient.Builder().
                    addInterceptor(logging)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .readTimeout(120L, TimeUnit.SECONDS)
                    .writeTimeout(120L, TimeUnit.SECONDS)
                    .addInterceptor(new Interceptor() {
                        @NotNull
                        @Override
                        public Response intercept(@NotNull Chain chain) throws IOException {
                            Request original = chain.request();

                            Request request = original.newBuilder()
                                    .header("Content-Type", "application/json")
                                    .method(original.method(), original.body())
                                    .build();

                            return chain.proceed(request);
                        }
                    }).build();

        retrofit = new Retrofit.Builder().client(mOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create()).
                        baseUrl(LoginInterface.BASE_URL).
                        build();
    }
}
