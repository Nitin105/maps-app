package com.example.mylocation.api;

public interface ApiHandler {

    void onApiSuccess(Object o, int objectType);

    void onApiError(String message);


}
