package com.example.mylocation;

import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mylocation.Utils.CommonUtils;
import com.example.mylocation.Utils.ConnectionDetector;
import com.example.mylocation.Utils.CostService;
import com.example.mylocation.api.ApiExtensions;
import com.example.mylocation.api.ApiHandler;
import com.example.mylocation.api.controller.LocationController;
import com.example.mylocation.data.RequestModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ApiHandler {

    private GoogleMap mMap;
    private Context mContext;
    private LocationController locationController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        CostService costService = new CostService(mContext);
        costService.startService();
        locationController = new LocationController(this, ApiExtensions.login.SEND_LOCATION.ordinal(),mContext);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng Mumbai = new LatLng(19.075, 72.877);
        mMap.addMarker(new MarkerOptions().position(Mumbai).title("Mumbai"));
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                Mumbai, 15);
        mMap.moveCamera(location);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                // Setting a click event handler for the map
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                    @Override
                    public void onMapClick(LatLng latLng) {

                        // Creating a marker
                        MarkerOptions markerOptions = new MarkerOptions();

                        // Setting the position for the marker
                        markerOptions.position(latLng);

                        // Setting the title for the marker.
                        // This will be displayed on taping the marker
                        markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                        // Clears the previously touched position
                        mMap.clear();

                        // Animating to the touched position
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                        // Placing a marker on the touched position
                        mMap.addMarker(markerOptions);

                        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        showDialog(addresses);
                    }
                });

            }
        });
    }

    private void showDialog(final List<Address> addresses){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.info);

        Address obj = addresses.get(0);
        String add = "1) Address : "+obj.getAddressLine(0);
        add = add + "\n2) country : " + obj.getCountryName();
        add = add + "\n3) Country Code : " + obj.getCountryCode();
        add = add + "\n4) state : " + obj.getAdminArea();
        add = add + "\n5) pinCode : " + obj.getPostalCode();
        add = add + "\n6) city : " + obj.getLocality();
        text.setText(add);

        TextView send = (TextView) dialog.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLoanApplication(addresses);
                dialog.dismiss();
            }
        });

        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();

    }

    private void getLoanApplication(final List<Address> addresses) {
        if (new ConnectionDetector().isNetworkAvailable(mContext)) {
            locationController.hitNow(getLoanApplicationJson(addresses), ApiExtensions.login.SEND_LOCATION.ordinal());
            CommonUtils.showLoading(mContext, "Loading..", false);
        } else {
            AlertDialog alertdialog = new AlertDialog.Builder(mContext).create();
            alertdialog.setCancelable(false);
            alertdialog.setTitle("Error");
            alertdialog.setMessage("No internet connection ");
            alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, "Retry", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    getLoanApplication(addresses);
                }
            });
            alertdialog.show();
        }
    }

    private RequestModel getLoanApplicationJson(final List<Address> addresses)
    {
        RequestModel model = new RequestModel();

        Address obj = addresses.get(0);
        model.setAddress(obj.getAddressLine(0));
        model.setCountry(obj.getCountryName());
        model.setCountryCode(obj.getCountryCode());
        model.setCity(obj.getLocality());
        model.setPinCode(obj.getPostalCode());
        model.setState(obj.getAdminArea());

        return  model;
    }

    @Override
    public void onApiSuccess(Object o, int objectType) {

        CommonUtils.hideLoading(mContext);
        switch (ApiExtensions.login.values()[objectType]) {
            case SEND_LOCATION:
                Toast.makeText(mContext,"Location Send Successfully",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onApiError(String message) {
        CommonUtils.hideLoading(mContext);
        CommonUtils.showdefault_alertdialog(mContext, "", message);
    }
}